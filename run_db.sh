#!/bin/bash

set -o allexport
source .env

docker run --name ramsis_test --env-file=.env -dp $PGPORT:$PGPORT postgres:14

set +o allexport