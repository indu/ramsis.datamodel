# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
Seismic forecast related ORM facilities.
"""
from sqlalchemy import (Column, Float, ForeignKey, Integer)
from sqlalchemy.orm import relationship

from ramsis.datamodel.base import (ORMBase, RealQuantityMixin,
                                   UniqueFiniteEpochMixin)


class ResultTimeBin(UniqueFiniteEpochMixin, ORMBase):
    modelrun_id = Column(Integer, ForeignKey('modelrun.id',
                                             name="fk_resulttimebin_modelrun",
                                             ondelete="CASCADE"))
    modelrun = relationship('ModelRun',
                            back_populates='resulttimebins')
    seismicforecastgrids = relationship('SeismicForecastGrid',
                                        back_populates='resulttimebin',
                                        cascade='all, delete-orphan',
                                        passive_deletes=True)
    seismicforecastcatalogs = relationship('SeismicForecastCatalog',
                                           back_populates='resulttimebin',
                                           cascade='all, delete-orphan',
                                           passive_deletes=True)

    def __repr__(self):
        return self._repr(id=self.id,
                          modelrun_id=self.modelrun_id,
                          starttime=self.starttime,
                          endtime=self.endtime,
                          number_catalogs=len(self.seismicforecastcatalogs),
                          number_grids=len(self.seismicforecastgrids))


class SeismicForecastGrid(ORMBase):
    resulttimebin_id = Column(Integer, ForeignKey(
        'resulttimebin.id',
        name="fk_seismicforecastgrid_resulttimebin",
        ondelete="CASCADE"))
    resulttimebin = relationship('ResultTimeBin',
                                 back_populates='seismicforecastgrids')

    seismicrates = relationship('SeismicRate',
                                back_populates='seismicforecastgrid',
                                cascade='all, delete-orphan',
                                passive_deletes=True)

    def __repr__(self):
        return self._repr(id=self.id,
                          number_rates=len(self.seismicrates))


class SeismicRate(RealQuantityMixin('numberevents'),
                  RealQuantityMixin('b'),
                  RealQuantityMixin('a'),
                  RealQuantityMixin('alpha'),
                  RealQuantityMixin('mc'),
                  ORMBase):
    """
    ORM representation of a seismicity forecast sample.
    """
    latitude_min = Column(Float)
    latitude_max = Column(Float)
    longitude_min = Column(Float)
    longitude_max = Column(Float)
    altitude_min = Column(Float)
    altitude_max = Column(Float)
    seismicforecastgrid_id = Column(Integer, ForeignKey(
        'seismicforecastgrid.id',
        name="fk_seismicrate_seismicforecastgrid",
        ondelete="CASCADE"))
    seismicforecastgrid = relationship('SeismicForecastGrid',
                                       back_populates='seismicrates')

    def __repr__(self):
        return self._repr(id=self.id,
                          a=self.a_value,
                          b=self.b_value,
                          mc=self.mc_value,
                          numberevents=self.numberevents_value,
                          latitude_min=self.latitude_min,
                          latitude_max=self.latitude_max,
                          longitude_min=self.longitude_min,
                          longitude_max=self.longitude_max,
                          altitude_min=self.altitude_min,
                          altitude_max=self.altitude_max
                          )
