# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
Model related ORM facilities.
"""
from datetime import datetime
import enum

from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import (Column, ForeignKey, Integer, String,
                        Boolean, DateTime, JSON, Enum)
from sqlalchemy.orm import relationship
from sqlalchemy.orm.exc import DetachedInstanceError

from ramsis.datamodel.association import (tag_forecast_series_association,
                                          tag_model_config_association)
from ramsis.datamodel.status import EStatus
from ramsis.datamodel.base import ORMBase, LogMixin
from ramsis.datamodel.type import GUID


class Tag(ORMBase):
    name = Column(String, unique=True)
    modelconfigs = relationship(
        'ModelConfig',
        back_populates='tags',
        secondary=tag_model_config_association)

    forecastseries = relationship(
        'ForecastSeries',
        back_populates='tags',
        secondary=tag_forecast_series_association)


class EResultType(enum.Enum):
    GRID = 0
    CATALOG = 1


class ModelConfig(ORMBase):
    name = Column(String, unique=True)
    url = Column(String)
    description = Column(String)
    enabled = Column(Boolean, default=True)
    result_type = Column(Enum(EResultType))

    # The model should be called by  sfm_module.sfm_function(*args)
    # by the ramsis worker.
    sfm_module = Column(String)
    sfm_function = Column(String)

    last_updated = Column(DateTime, default=datetime.utcnow(),
                          onupdate=datetime.utcnow())
    # config would include epoch duration and geometry resolution
    # if required
    config = Column(JSON, nullable=False)

    runs = relationship('ModelRun',
                        back_populates='modelconfig',
                        cascade='all, delete-orphan',
                        passive_deletes=True)
    tags = relationship(
        'Tag',
        back_populates='modelconfigs',
        secondary=tag_model_config_association)

    def __repr__(self):
        tags = [tag.name for tag in self.tags]
        return '<%s(name=%s, url=%s, id=%s, tags=%s, enabled=%s)>' % (
            type(self).__name__, self.name,
            self.url, self.id, tags, self.enabled)

    @hybrid_property
    def forecasts(self):
        configs = []
        for tag in self.tags:
            configs.extend(tag.forecasts)
        return list(set(configs))

    @forecasts.expression
    def forecasts(cls):
        configs = []
        for tag in cls.tags:
            configs.extend(tag.forecasts)
        return list(set(configs))


class ModelRun(LogMixin, ORMBase):
    """
    ORM representation of a seismicity forecast model run.
    """
    status = Column(Enum(EStatus))

    runid = Column(GUID, unique=True, index=True)

    modelconfig_id = Column(Integer,
                            ForeignKey('modelconfig.id',
                                       ondelete="CASCADE"))
    modelconfig = relationship('ModelConfig',
                               back_populates='runs')

    forecast_id = Column(Integer, ForeignKey('forecast.id',
                                             ondelete="CASCADE"))
    forecast = relationship('Forecast',
                            back_populates='runs')

    resulttimebins = relationship('ResultTimeBin',
                                  back_populates='modelrun',
                                  cascade='all, delete-orphan',
                                  passive_deletes=True)

    task = relationship("Task", back_populates='modelrun')
    injectionplan_id = Column(Integer, ForeignKey('injectionplan.id'))
    injectionplan = relationship('InjectionPlan',
                                 back_populates='injectionplan')

    def clone(self, with_results=False):
        """
        Clone a seismicity model run.

        :param bool with_results: If :code:`True`, append results and related
            data while cloning, otherwise results are excluded.
        """
        new = super().clone(with_results=with_results)
        new.modelconfig = self.modelconfig
        new.runid = None
        return new

    def __repr__(self):
        return '<%s(name=%s, url=%s)>' % (type(self).__name__,
                                          self.modelconfig.name,
                                          self.id)

    def snapshot(self):
        def try_detached(attr_name, snap):
            try:
                setattr(snap, attr_name, getattr(self, attr_name))
            except DetachedInstanceError:
                pass
            return snap

        snap = type(self)()
        for attr_name in ["modelconfig",
                          "modelconfig_id",
                          "forecast_id",
                          "forecast",
                          "runid"]:
            snap = try_detached(attr_name, snap)
        return snap
