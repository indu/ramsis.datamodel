
# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
Injection Plan related ORM facilities.
"""

from sqlalchemy import Column, ForeignKey, Integer, LargeBinary
from ramsis.datamodel.base import (NameMixin, ORMBase)
from sqlalchemy.orm import relationship


class InjectionPlan(NameMixin, ORMBase):
    data = Column(LargeBinary)
    forecastseries_id = Column(Integer, ForeignKey('forecastseries.id',
                               ondelete="CASCADE"))
    forecastseries = relationship(
        'ForecastSeries',
        back_populates='injectionplans')
    injectionplan = relationship('ModelRun')
