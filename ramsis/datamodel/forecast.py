# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
Forecast related ORM facilities.
"""
from sqlalchemy import Column, ForeignKey, Integer, LargeBinary, Enum
from sqlalchemy.types import Boolean
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship

from ramsis.datamodel.base import (CreationInfoMixin, UniqueOpenEpochMixin,
                                   EpochMixin, NameMixin, GeometryExtentMixin,
                                   ORMBase, LogMixin)
from ramsis.datamodel.status import EStatus
from ramsis.datamodel.utils import clone
from ramsis.datamodel.association import \
    tag_forecast_series_association


class ForecastSeries(
        CreationInfoMixin,
        UniqueOpenEpochMixin,
        NameMixin, GeometryExtentMixin, LogMixin, ORMBase):
    active = Column(Boolean)
    status = Column(Enum(EStatus))

    # relation: Project
    project_id = Column(Integer, ForeignKey('project.id', ondelete="CASCADE"))
    project = relationship('Project',
                           back_populates='forecastseries')

    forecasts = relationship('Forecast',
                             back_populates='forecastseries',
                             cascade='all, delete-orphan',
                             passive_deletes=True)
    tags = relationship(
        'Tag',
        back_populates='forecastseries',
        secondary=tag_forecast_series_association)

    injectionplans = relationship('InjectionPlan',
                                  back_populates='forecastseries',
                                  cascade='all, delete-orphan',
                                  passive_deletes=True)

    # Interval in seconds to place forecasts apart in time.
    forecastinterval = Column(Integer)
    forecastduration = Column(Integer)

    @hybrid_property
    def model_configs(self):
        configs = []
        for tag in self.tags:
            configs.extend(tag.modelconfigs)
        return list(set(configs))

    @model_configs.expression
    def model_configs(cls):
        configs = []
        for tag in cls.tags:
            configs.extend(tag.modelconfigs)
        return list(set(configs))


class Forecast(CreationInfoMixin,
               EpochMixin('interval', epoch_type='finite', column_prefix=''),
               NameMixin, LogMixin, ORMBase):
    status = Column(Enum(EStatus))

    forecastseries_id = Column(Integer, ForeignKey('forecastseries.id',
                               ondelete="CASCADE"))
    forecastseries = relationship('ForecastSeries',
                                  back_populates='forecasts')

    seismiccatalog = Column(LargeBinary)
    injectionwell = Column(LargeBinary)

    runs = relationship('ModelRun',
                        back_populates='forecast',
                        cascade='all, delete-orphan',
                        passive_deletes=True)

    @hybrid_property
    def duration(self):
        return self.endtime - self.starttime

    def clone(self, with_data=False):
        """
        Clone a forecast.

        :param bool with_data: If :code:`True`, append related
            data while cloning, otherwise results are excluded.
        """
        new = clone(self, with_foreignkeys=False)
        new.status = EStatus.PENDING
        if with_data:
            new.seismiccatalog = self.seismiccatalog
            new.injectionwell = self.injectionwell

        return new

    @hybrid_property
    def model_configs(self):
        configs = []
        for tag in self.forecastseries.tags:
            configs.extend(tag.modelconfigs)
        return list(set(configs))

    @model_configs.expression
    def model_configs(cls):
        configs = []
        for tag in cls.forecastseries.tags:
            configs.extend(tag.modelconfigs)
        return list(set(configs))
