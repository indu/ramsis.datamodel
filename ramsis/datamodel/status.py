# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
Processing status related ORM facilities.
"""

import enum


class EStatus(enum.Enum):
    PENDING = 0
    SCHEDULED = 1
    PAUSED = 2
    RUNNING = 3
    CANCELLED = 4
    FAILED = 5
    COMPLETED = 6
