# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
General purpose datamodel ORM facilities.
"""
import typing
import datetime
import enum
import functools
from pydantic import parse_obj_as

from sqlalchemy.dialects.postgresql import JSONB


from sqlalchemy import (Boolean, Column, DateTime, Float, Integer, String,
                        event, inspect, types)
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.ext.mutable import MutableList
from sqlalchemy.orm import Session, validates, class_mapper
from sqlalchemy.orm.exc import DetachedInstanceError
from sqlalchemy.types import ARRAY, PickleType


class Base(object):

    id = Column(Integer, primary_key=True)

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    def __repr__(self) -> str:
        return self._repr(id=self.id)

    def _repr(self, **fields: typing.Dict[str, typing.Any]) -> str:
        '''
        Helper for __repr__
        '''
        field_strings = []
        at_least_one_attached_attribute = False
        for key, field in fields.items():
            try:
                field_strings.append(f'{key}={field!r}')
            except DetachedInstanceError:
                field_strings.append(f'{key}=DetachedInstanceError')
            else:
                at_least_one_attached_attribute = True
        if at_least_one_attached_attribute:
            return f"<{self.__class__.__name__}({','.join(field_strings)})>"
        return f"<{self.__class__.__name__} {id(self)}>"

    def __eq__(self, other):
        if isinstance(other, type(self)):
            mapper = class_mapper(type(self))

            pk_keys = set([c.key for c in mapper.primary_key])
            rel_keys = set([c.key for c in mapper.relationships])
            fk_keys = set([c.key for c in mapper.columns if c.foreign_keys])

            omit = pk_keys | rel_keys | fk_keys

            return all(getattr(self, attr) == getattr(other, attr)
                       for attr in [p.key for p in mapper.iterate_properties
                                    if p.key not in omit])

            raise ValueError("Class types are different: "
                             f"{type(self)} {type(other)}")

    def __hash__(self):
        return id(self)


ORMBase = declarative_base(cls=Base)

# SQLite has no ARRAY type. Since we will probably only work with sqlite
# on a programmatic basis, it should be no problem to save the lists pickled
CompatibleFloatArray = \
    ARRAY(Float).with_variant(MutableList.as_mutable(PickleType), 'sqlite')

# ----------------------------------------------------------------------------
# XXX(damb): Within the mixins below the QML type *ResourceReference* (i.e. an
# URI) is implemented as sqlalchemy.String


class LogMixin:
    log = Column(MutableList.as_mutable(ARRAY(String)), default=[])

    def add_log(self, item):
        self.log.append(
            f"{datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S')}: "
            f"{item}")


class CreationInfoMixin(object):
    """
    `SQLAlchemy <https://www.sqlalchemy.org/>`_ mixin emulating type
    :code:`CreationInfo` from `QuakeML <https://quake.ethz.ch/quakeml/>`_.
    """
    creationinfo_author = Column(String)
    creationinfo_authoruri_resourceid = Column(String)
    creationinfo_authoruri_used = Column(Boolean)
    creationinfo_agencyid = Column(String)
    creationinfo_agencyuri_resourceid = Column(String)
    creationinfo_agencyuri_used = Column(Boolean)
    creationinfo_creationtime = Column(DateTime,
                                       default=datetime.datetime.utcnow())
    creationinfo_version = Column(String)
    creationinfo_copyrightowner = Column(String)
    creationinfo_copyrightowneruri_resourceid = Column(String)
    creationinfo_copyrightowneruri_used = Column(Boolean)
    creationinfo_license = Column(String)


class NameMixin(object):
    """
    `SQLAlchemy <https://www.sqlalchemy.org/>`_ mixin providing a general
    purpose :code:`name` attribute.
    """
    name = Column(String)


class GeometryExtentMixin:
    boundingpolygon = Column(String)
    altitudemin = Column(Float)
    altitudemax = Column(Float)


def _PublicIDMixin(name='', column_prefix=None):
    """
    `SQLAlchemy <https://www.sqlalchemy.org/>`_ mixin providing a general
    purpose :code:`publicID` attribute.

    .. note::

        The attribute :code:`publicID` is inherited from `QuakeML
        <https://quake.ethz.ch/quakeml/>`_.
    """
    if not column_prefix:
        column_prefix = '%s' % name

    @declared_attr
    def _publicid(cls):
        return Column('%spublicid' % column_prefix, String)

    return type(name, (object,), {'%spublicid' % column_prefix: _publicid})


PublicIDMixin = _PublicIDMixin()


def EpochMixin(name, epoch_type=None, column_prefix=None):
    """
    Mixin factory for common :code:`Epoch` types from
    `QuakeML <https://quake.ethz.ch/quakeml/>`_.

    Epoch types provide the fields `starttime` and `endtime`. Note, that a
    `column_prefix` may be prepended.

    :param str name: Name of the class returned
    :param epoch_type: Type of the epoch to be returned. Valid values
        are :code:`None` or :code:`default`, :code:`open` and :code:`finite`.
    :type epoch_type: str or None
    :param column_prefix: Prefix used for DB columns. If :code:`None`, then
        :code:`name` with an appended underscore :code:`_` is used. Capital
        letters are converted to lowercase.
    :type column_prefix: str or None

    The usage of :py:func:`EpochMixin` is illustrated bellow:

    .. code::

        import datetime

        # define a ORM mapping using the "Epoch" mixin factory
        class MyObject(EpochMixin('epoch'), ORMBase):

            def __repr__(self):
                return \
                    '<MyObject(epoch_starttime={}, epoch_endtime={})>'.format(
                        self.epoch_starttime, self.epoch_endtime)


        # create instance of "MyObject"
        my_obj = MyObject(epoch_starttime=datetime.datetime.utcnow())

    """
    if column_prefix is None:
        column_prefix = '%s_' % name

    column_prefix = column_prefix.lower()

    class Boundary(enum.Enum):
        LEFT = enum.auto()
        RIGHT = enum.auto()

    def create_datetime(boundary, column_prefix, **kwargs):

        def _make_datetime(boundary, **kwargs):

            if boundary is Boundary.LEFT:
                name = 'starttime'
            elif boundary is Boundary.RIGHT:
                name = 'endtime'
            else:
                raise ValueError('Invalid boundary: {!r}.'.format(boundary))

            @declared_attr
            def _datetime(cls):
                return Column('%s%s' % (column_prefix, name), DateTime,
                              **kwargs)

            return _datetime

        return _make_datetime(boundary, **kwargs)

    if epoch_type is None or epoch_type == 'default':
        _func_map = (('starttime', create_datetime(Boundary.LEFT,
                                                   column_prefix,
                                                   nullable=False)),
                     ('endtime', create_datetime(Boundary.RIGHT,
                                                 column_prefix)))
    elif epoch_type == 'open':
        _func_map = (('starttime', create_datetime(Boundary.LEFT,
                                                   column_prefix)),
                     ('endtime', create_datetime(Boundary.RIGHT,
                                                 column_prefix)))
    elif epoch_type == 'finite':
        _func_map = (('starttime', create_datetime(Boundary.LEFT,
                                                   column_prefix,
                                                   nullable=False)),
                     ('endtime', create_datetime(Boundary.RIGHT,
                                                 column_prefix,
                                                 nullable=False)))
    else:
        raise ValueError('Invalid epoch_type: {!r}.'.format(epoch_type))

    def __dict__(func_map, attr_prefix):

        return {'{}{}'.format(attr_prefix, attr_name): attr
                for attr_name, attr in func_map}

    return type(name, (object,), __dict__(_func_map, column_prefix))


UniqueEpochMixin = EpochMixin('Epoch', column_prefix='')
UniqueOpenEpochMixin = EpochMixin('Epoch', epoch_type='open',
                                  column_prefix='')
UniqueFiniteEpochMixin = EpochMixin('Epoch', epoch_type='finite',
                                    column_prefix='')


class ValidatedEpochMixin(UniqueOpenEpochMixin):
    """
    Mixin class that validates starttime and endtime
    in the ORM rather than the database. This should
    be used for single table inheritance because subclasses
    should not contain nullable=False columns.
    """
    @validates('starttime', 'endtime')
    def validate_starttime(self, key, value):
        assert value is not None
        return value


def QuantityMixin(name, quantity_type, column_prefix=None, optional=True,
                  index=False):
    """
    Mixin factory for common :code:`Quantity` types from
    `QuakeML <https://quake.ethz.ch/quakeml/>`_.

    Quantity types provide the fields:
        - `value`
        - `uncertainty`
        - `loweruncertainty`
        - `upperuncertainty`
        - `confidencelevel`

    Note, that a `column_prefix` may be prepended.

    :param str name: Name of the class returned
    :param str quantity_type: Type of the quantity to be returned. Valid values
        are :code:`int`, :code:`real` or rather :code:`float` and :code:`time`.
    :param column_prefix: Prefix used for DB columns. If :code:`None`, then
        :code:`name` with an appended underscore :code:`_` is used. Capital
        Letters are converted to lowercase.
    :type column_prefix: str or None
    :param bool optional: Flag making the :code:`value` field optional
        (:code:`True`).

    The usage of :py:func:`QuantityMixin` is illustrated bellow:

    .. code::

        # define a ORM mapping using the Quantity mixin factory
        class FooBar(QuantityMixin('foo', 'int'),
                     QuantityMixin('bar', 'real'),
                     ORMBase):

            def __repr__(self):
                return '<FooBar (foo_value=%d, bar_value=%f)>' % (
                    self.foo_value, self.bar_value)


        # create instance of "FooBar"
        foobar = FooBar(foo_value=1, bar_value=2)

    """

    if column_prefix is None:
        column_prefix = '%s_' % name

    column_prefix = column_prefix.lower()

    def create_value(quantity_type, column_prefix, optional):

        def _make_value(sql_type, column_prefix, optional):

            @declared_attr
            def _value(cls):
                return Column('%svalue' % column_prefix, sql_type,
                              nullable=optional, index=index)

            return _value

        if quantity_type == 'int':
            return _make_value(Integer, column_prefix, optional)
        elif quantity_type in ('real', 'float'):
            return _make_value(Float, column_prefix, optional)
        elif quantity_type == 'time':
            return _make_value(DateTime, column_prefix, optional)

        raise ValueError('Invalid quantity_type: {}'.format(quantity_type))

    @declared_attr
    def _uncertainty(cls):
        return Column('%suncertainty' % column_prefix, Float)

    @declared_attr
    def _lower_uncertainty(cls):
        return Column('%sloweruncertainty' % column_prefix, Float)

    @declared_attr
    def _upper_uncertainty(cls):
        return Column('%supperuncertainty' % column_prefix, Float)

    @declared_attr
    def _confidence_level(cls):
        return Column('%sconfidencelevel' % column_prefix, Float)

    _func_map = (('value',
                  create_value(quantity_type, column_prefix, optional)),
                 ('uncertainty', _uncertainty),
                 ('loweruncertainty', _lower_uncertainty),
                 ('upperuncertainty', _upper_uncertainty),
                 ('confidencelevel', _confidence_level),
                 )

    def __dict__(func_map, attr_prefix):

        return {'{}{}'.format(attr_prefix, attr_name): attr
                for attr_name, attr in func_map}

    return type(name, (object,), __dict__(_func_map, column_prefix))


FloatQuantityMixin = functools.partial(QuantityMixin,
                                       quantity_type='float')
RealQuantityMixin = FloatQuantityMixin
IntegerQuantityMixin = functools.partial(QuantityMixin,
                                         quantity_type='int')
TimeQuantityMixin = functools.partial(QuantityMixin,
                                      quantity_type='time')


def DeleteMultiParentOrphanMixin(parent_relationships):
    """
    This provides a Mixin for entities that have multiple possible owners
    (parents). For these we can't simply configure the owning relationships
    cascading option as 'delete-orphan'. Instead we install a listener on the
    session that deletes any orphaned catalogs after a flush.

    :param [str] parent_relationships: Relationship attributes that define
        the relations to parent entities.
    """

    class Mixin:

        @classmethod
        def __declare_last__(cls):
            """ Called by the ORM after mapping has completed """

            def is_orphaned(i):
                return not any(getattr(i, r) for r in parent_relationships)

            @event.listens_for(Session, 'after_flush')
            def delete_orphans(session, _):
                for orphan in (i for i in session.dirty | session.new
                               if isinstance(i, cls) and is_orphaned(i)
                               and inspect(i).persistent):
                    session.delete(orphan)

    return Mixin


class PydanticType(types.TypeDecorator):
    """Pydantic type.
    Uses the dict to create a pydantic model.
    https://roman.pt/posts/pydantic-in-sqlalchemy-fields/
    """
    # Ref: https://www.postgresql.org/docs/13/datatype-json.html
    impl = JSONB

    def __init__(self, pydantic_type):
        super().__init__()
        self.pydantic_type = pydantic_type

    def load_dialect_impl(self, dialect):
        # Use JSONB for PostgreSQL and JSON for other databases.
        return dialect.type_descriptor(JSONB())

    def process_bind_param(self, value, dialect):
        return value.dict() if value else None

    def process_result_value(self, value, dialect):
        return parse_obj_as(self.pydantic_type, value) if value else None
