import os

import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv

from ramsis.datamodel import ORMBase

load_dotenv()

SQLALCHEMY_DATABASE_URL = (
    f"postgresql://{os.environ['POSTGRES_USER']}:"
    f"{os.environ['POSTGRES_PASSWORD']}@"
    f"{os.environ['POSTGRES_SERVER']}:"
    f"{os.environ['PGPORT']}/{os.environ['POSTGRES_DB']}")


engine = create_engine(SQLALCHEMY_DATABASE_URL)
Session = sessionmaker()


@pytest.fixture(scope='session')
def connection():
    connection = engine.connect()
    yield connection
    connection.close()


@pytest.fixture(scope="module")
def setup_database(connection):
    ORMBase.metadata.bind = connection
    ORMBase.metadata.create_all()

    yield

    ORMBase.metadata.drop_all()


@pytest.fixture(scope='class')
def session(setup_database, connection):
    transaction = connection.begin()
    session = Session(bind=connection)

    yield session

    session.close()
    transaction.rollback()
