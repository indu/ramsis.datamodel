# Copyright (C) 2019, ETH Zurich - Swiss Seismological Service SED
"""
Seismics related test facilities.
"""

from dateutil import parser
import datetime
import unittest

import ramsis.datamodel as dm
from ramsis.datamodel.status import EStatus


class ForecastTestCase(unittest.TestCase):
    """
    Test cases for :py:class:`ramsis.datamodel.forecast.Forecast`.
    """

    def test_clone_with_data(self):
        model = dm.ModelConfig(
            name='EM1.1',
            enabled=True,
            sfm_module="module",
            sfm_function="function",
            url='http://localhost:5000',
            config={'foo': 42})

        r0 = dm.SeismicRate(
            latitude_min=20.0,
            latitude_max=30.0,
            longitude_min=30.0,
            longitude_max=40.0,
            altitude_min=-10.0,
            altitude_max=-10.0,
            b_value=73,
            a_value=1.,
            mc_value=1.5,
            numberevents_value=42.0)

        s0 = dm.SeismicForecastGrid(
            seismicrates=[r0, ])

        t0 = dm.ResultTimeBin(
            starttime=parser.parse(
                '2019-07-02T14:59:52.508142'),
            endtime=parser.parse('2019-07-02T14:59:52.508142'),
            seismicforecastgrids=[s0, ])

        model_run = dm.ModelRun(
            modelconfig=model,
            status=EStatus.COMPLETED,
            resulttimebins=[t0])

        fc = dm.Forecast(name='Forecast',
                         runs=[model_run],
                         starttime=datetime.datetime.utcnow(),
                         endtime=datetime.datetime.utcnow(),
                         seismiccatalog="",
                         injectionwell=[]
                         )

        cloned = fc.clone(with_data=True)
        self.assertEqual(cloned.runs, [])
