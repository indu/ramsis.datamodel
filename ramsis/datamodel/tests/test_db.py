# # Copyright (C) 2019, ETH Zurich - Swiss Seismological Service SED
# """
# General purpose DB test facilities.
# """
# from sqlalchemy import select
#
# from ramsis.datamodel import Project
#
#
# class TestDBSetup:
#     def test_create_project(self, session):
#         project = Project(id=1)
#         session.add(project)
#         session.commit()
#         stmt = select(Project).where(Project.id == 1)
#         project_db = session.scalar(stmt)
#         assert project == project_db
#
#     def test_new(self, session):
#         stmt = select(Project).where(Project.id == 1)
#         project_db = session.scalar(stmt)
#         assert project_db
#
#
# def test_new2(session):
#     stmt = select(Project).where(Project.id == 1)
#     project_db = session.scalar(stmt)
#     assert project_db is None
