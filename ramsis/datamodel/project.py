# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
Project related ORM facilities.
"""

from sqlalchemy import Column, String, Enum, LargeBinary
from sqlalchemy.orm import relationship
import enum

from ramsis.datamodel.base import (CreationInfoMixin, NameMixin, ORMBase,
                                   UniqueOpenEpochMixin)


class EInput(enum.Enum):
    REQUIRED = 0
    OPTIONAL = 1
    NOT_ALLOWED = 2


class Project(CreationInfoMixin, NameMixin, UniqueOpenEpochMixin, ORMBase):
    """
    RT-RAMSIS project ORM representation. :py:class:`Project` is the root
    object of the RT-RAMSIS data model.
    """
    description = Column(String)

    # seismiccatalog and injectionwell only populated here if this data
    # is given at forecastseries creation time. Otherwise
    # URLs are used
    seismiccatalog = Column(LargeBinary)
    injectionwell = Column(LargeBinary)

    forecastseries = relationship('ForecastSeries',
                                  order_by='ForecastSeries.starttime',
                                  back_populates='project',
                                  cascade='all, delete-orphan',
                                  passive_deletes=True)

    fdsnws_url = Column(String, nullable=True)
    hydws_url = Column(String, nullable=True)
    seismiccatalog_required = Column(Enum(EInput), default=EInput.REQUIRED)
    injectionwell_required = Column(Enum(EInput), default=EInput.REQUIRED)
    injectionplan_required = Column(Enum(EInput), default=EInput.REQUIRED)
