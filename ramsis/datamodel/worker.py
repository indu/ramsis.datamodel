# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
Worker related classes. These should not be used when creating
the rt-ramsis database, only for ramsis.worker
"""

import datetime
import enum

from sqlalchemy import Column, DateTime, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import relationship

from ramsis.datamodel.base import ORMBase
from ramsis.datamodel.model import ModelRun
from ramsis.datamodel.type import GUID


class StatusCode(enum.Enum):
    """
    SFM-Worker status code enum.
    """
    # codes related to worker states
    TaskAccepted = 202
    TaskProcessing = 423
    TaskError = 418
    TaskCompleted = 200
    TaskNotAvailable = 204
    # codes related to worker resource
    HTTPMethodNotAllowed = 405
    UnprocessableEntity = 422
    WorkerError = 500


class LastSeenMixin(object):

    @declared_attr
    def lastseen(cls):
        return Column(DateTime, default=datetime.datetime.utcnow,
                      onupdate=datetime.datetime.utcnow)


class Task(LastSeenMixin, ORMBase):
    """
    RAMSIS worker task ORM mapping.
    """
    id = Column(GUID, unique=True, index=True, primary_key=True)

    status = Column(String, nullable=False)
    status_code = Column(Integer, nullable=False)
    warning = Column(String)
    modelrun_id = Column(Integer, ForeignKey('modelrun.id'))
    modelrun = relationship("ModelRun",
                            single_parent=True,
                            cascade='all, delete-orphan',
                            back_populates="task")

    @classmethod
    def new(cls, id, modelconfig):
        model_run = ModelRun(modelconfig=modelconfig)
        return cls(id=id, status=StatusCode.TaskAccepted.name,
                   status_code=StatusCode.TaskAccepted.value,
                   modelrun=model_run)

    @classmethod
    def pending(cls, id, modelconfig):
        return cls(id=id, status=StatusCode.TaskProcessing.name,
                   status_code=StatusCode.TaskProcessing.value,
                   modelconfig=modelconfig)

    def __repr__(self):
        return "<{}(id={})>".format(type(self).__name__, self.id)
