
from sqlalchemy import Column, ForeignKey, Integer, Table

from ramsis.datamodel.base import ORMBase


tag_model_config_association = Table(
    'tag_model_config_association', ORMBase.metadata,
    Column('tag_id', Integer,
           ForeignKey('tag.id', ondelete="CASCADE"), primary_key=True),
    Column('modelconfig_id', Integer,
           ForeignKey('modelconfig.id', ondelete="CASCADE"), primary_key=True))

tag_forecast_series_association = Table(
    'tag_forecast_series_association', ORMBase.metadata,
    Column('forecastseries_id', Integer,
           ForeignKey('forecastseries.id', ondelete="CASCADE"),
           primary_key=True),
    Column('tag_id', Integer,
           ForeignKey('tag.id', ondelete="CASCADE"), primary_key=True))
