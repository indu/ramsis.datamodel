# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
Utilities for SFM-Worker data import/export.
"""
import base64
import functools
import re
import json
from functools import partial
import dateutil.parser
from marshmallow import (Schema, fields, post_dump, post_load,
                         pre_dump, pre_load, validate)

from ramsis.io.seismics import (_SeismicForecastCatalogSchema)
from ramsis.datamodel.seismicity import (ResultTimeBin,
                                         SeismicForecastGrid,
                                         SeismicRate)
from ramsis.io.utils import (DateTime, DeserializerBase, IOBase, Percentage,
                             SerializerBase, Uncertainty,
                             _IOError, append_ms_zeroes)


def validate_positive(d):
    return d >= 0


validate_percentage = validate.Range(min=0, max=100)
validate_longitude = validate.Range(min=-180., max=180.)
validate_latitude = validate.Range(min=-90., max=90)
validate_ph = validate.Range(min=0, max=14)

Positive = functools.partial(fields.Float, validate=validate_positive)

Latitude = partial(fields.Float, validate=validate_latitude)
RequiredLatitude = partial(Latitude, required=True)
Longitude = partial(fields.Float, validate=validate_longitude)
RequiredLongitude = partial(Longitude, required=True)
FluidPh = partial(fields.Float, validate=validate_ph)


# from marshmallow (originally from Django)
_iso8601_re = re.compile(
    r'(?P<year>\d{4})-(?P<month>\d{1,2})-(?P<day>\d{1,2})'
    r'[T ](?P<hour>\d{1,2}):(?P<minute>\d{1,2})'
    r'(?::(?P<second>\d{1,2})(?:\.(?P<microsecond>\d{1,6})\d{0,6})?)?'
    # tzinfo must not be available
    r'(?P<tzinfo>(?!\Z|[+-]\d{2}(?::?\d{2})?))?$')


class _SchemaBase(Schema):
    """
    Schema base class.
    """
    class Meta:
        ordered = True
        strict = True

    @classmethod
    def _clear_missing(cls, data):
        return dict((k, v) for k, v in data.items()
                    if (v and v not in ([], {}))
                    or isinstance(v, (int, float)))

    @classmethod
    def _flatten_dict(cls, data, sep='_'):
        """
        Flatten a a nested dict :code:`dict` using :code:`sep` as key
        separator.
        """
        retval = {}
        for k, v in data.items():

            if isinstance(v, dict):
                sub_k = list(v.keys())[0]
                if sub_k in ['value', 'uncertainty', 'upperuncertainty',
                             'loweruncertainty', 'confidencelevel']:
                    for sub_k, sub_v in cls._flatten_dict(v, sep).items():
                        retval[k + sep + sub_k] = sub_v
                else:
                    retval[k] = v
            else:
                retval[k] = v

        return retval

    @classmethod
    def _nest_dict(cls, data, sep='_'):
        """
        Nest a dictionary by splitting the key on a delimiter.
        """
        retval = {}
        for k, v in data.items():
            t = retval
            prev = None
            if k in ['status_code', 'latitude_min',
                     'latitude_max', 'longitude_min', 'longitude_max',
                     'altitude_min', 'altitude_max']:
                t.setdefault(k, v)
                continue
            for part in k.split(sep):
                if prev is not None:
                    t = t.setdefault(prev, {})
                prev = part
            else:
                t.setdefault(prev, v)

        return retval

    @classmethod
    def _append_zero_milliseconds(cls, data):
        # XXX(damb): This is a workaround since the DateTime format is being
        # configured with a date string. Is there a better solution provided by
        # marshmallow.
        if 'starttime' in data:
            data['starttime'] = append_ms_zeroes(data['starttime'])

        if 'endtime' in data:
            data['endtime'] = append_ms_zeroes(data['endtime'])
        return data


def datetime_to_isoformat(dt, localtime=False, *args, **kwargs):
    """
    Convert a :py:class:`datetime.datetime` object to a ISO8601 conform string.
    :param datetime.datetime dt: Datetime object to be converted
    :param bool localtime: The parameter is ignored
    :returns: ISO8601 conform datetime string
type: str
    """
    # ignores localtime parameter
    return dt.isoformat(*args, **kwargs)


def string_to_datetime(datestring, use_dateutil=True):
    """
    Parse a datestring from a string specified by the FDSNWS datetime
    specification.
    :param str datestring: String to be parsed
    :param bool use_dateutil: Make use of the :code:`dateutil` package if set
        to :code:`True`
    :returns: Datetime
    :rtype: :py:class:`datetime.datetime`
    See: http://www.fdsn.org/webservices/FDSN-WS-Specifications-1.1.pdf
    """
    IGNORE_TZ = True

    if not _iso8601_re.match(datestring):
        raise ValueError('Not a valid ISO8601-formatted string.')
    return dateutil.parser.parse(datestring, ignoretz=IGNORE_TZ)


class SFMWIOError(_IOError):
    """Base SFMW de-/serialization error ({})."""


# Model Output
class _SeismicRateSchema(_SchemaBase):
    latitude_min = RequiredLatitude()
    latitude_max = RequiredLatitude()
    longitude_min = RequiredLongitude()
    longitude_max = RequiredLongitude()
    altitude_min = fields.Float()
    altitude_max = fields.Float()

    numberevents_value = fields.Float()
    numberevents_uncertainty = Uncertainty()
    numberevents_loweruncertainty = Uncertainty()
    numberevents_upperuncertainty = Uncertainty()
    numberevents_confidencelevel = Percentage()

    b_value = fields.Float()
    b_uncertainy = Uncertainty()
    b_loweruncertainty = Uncertainty()
    b_upperuncertainty = Uncertainty()
    b_confidencelevel = Percentage()

    a_value = fields.Float()
    a_uncertainy = Uncertainty()
    a_loweruncertainty = Uncertainty()
    a_upperuncertainty = Uncertainty()
    a_confidencelevel = Percentage()

    alpha_value = fields.Float()
    alpha_uncertainy = Uncertainty()
    alpha_loweruncertainty = Uncertainty()
    alpha_upperuncertainty = Uncertainty()
    alpha_confidencelevel = Percentage()

    mc_value = fields.Float()
    mc_uncertainy = Uncertainty()
    mc_loweruncertainty = Uncertainty()
    mc_upperuncertainty = Uncertainty()
    mc_confidencelevel = Percentage()

    @pre_load
    def preload(self, data, **kwargs):
        data = self._append_zero_milliseconds(data)
        data = self._flatten_dict(data)
        return data

    @post_load
    def postload(self, data, **kwargs):
        if (self.context.get('format') == 'dict'):
            return data
        return SeismicRate(**data)

    @post_dump
    def postdump(self, data, **kwargs):
        filtered_data = self._clear_missing(data)
        nested_data = self._nest_dict(filtered_data, sep='_')
        return nested_data


class _SeismicForecastGridSchema(_SchemaBase):

    seismicrates = fields.Nested(_SeismicRateSchema, many=True)

    @post_load
    def post_load_data(self, data, **kwargs):
        return SeismicForecastGrid(**data)


class _ResultTimeBinSchema(_SchemaBase):
    starttime = DateTime(required=True)
    endtime = DateTime(required=True)
    seismicforecastgrids = fields.Nested(_SeismicForecastGridSchema(
        many=True))
    seismicforecastcatalogs = fields.Nested(
        _SeismicForecastCatalogSchema, many=True)

    @post_load
    def post_load_data(self, data, **kwargs):
        return ResultTimeBin(**data)


class UTCDateTime(fields.DateTime):
    """
    The class extends marshmallow standard DateTime with a FDSNWS *datetime*
    format.
    The FDSNWS *datetime* format is described in the `FDSN Web Service
    Specifications
    <http://www.fdsn.org/webservices/FDSN-WS-Specifications-1.1.pdf>`_.
    """

    SERIALIZATION_FUNCS = fields.DateTime.SERIALIZATION_FUNCS.copy()

    DESERIALIZATION_FUNCS = fields.DateTime.DESERIALIZATION_FUNCS.copy()

    SERIALIZATION_FUNCS['utc_isoformat'] = datetime_to_isoformat
    DESERIALIZATION_FUNCS['utc_isoformat'] = string_to_datetime


class _GeometryExtentSchema(_SchemaBase):
    bounding_polygon = fields.String(required=True)
    altitude_min = fields.Float(required=False)
    altitude_max = fields.Float(required=False)

    @post_dump
    def clear_missing(self, data, **kwargs):
        return self._clear_missing(data)


class _SFMWorkerRunsAttributesSchema(_SchemaBase):
    model_parameters = fields.Dict(required=True)
    geometry = fields.Nested(_GeometryExtentSchema, required=True)
    injection_well = fields.List(fields.Dict(), allow_none=True)
    injection_plan = fields.Dict(keys=fields.Str(), allow_none=True)
    forecast_start = UTCDateTime('utc_isoformat')
    forecast_end = UTCDateTime('utc_isoformat')
    seismic_catalog = fields.String(required=True, allow_none=True)

    @post_dump
    def postdump(self, data, **kwargs):
        """
        Encode the catalog using base64 encoding.
        """
        if 'seismic_catalog' in data:
            data['seismic_catalog'] = base64.b64encode(
                data['seismic_catalog'].encode('utf8')).decode('utf8')
        return self._clear_missing(data)

    @pre_load
    def preload(self, data, **kwargs):
        if 'seismic_catalog' in data.keys():
            data['seismic_catalog'] = base64.b64decode(
                data['seismic_catalog'])
        return data

    @pre_dump
    def serialize_data(self, data, **kwargs):
        if 'injection_plan' in data.keys():
            if data["injection_plan"]:
                data["injection_plan"] = json.loads(data["injection_plan"].
                                                    decode('utf-8'))
        if 'injection_well' in data.keys():
            if data["injection_well"]:
                data["injection_well"] = json.loads(data["injection_well"].
                                                    decode('utf-8'))
        return data


class _SFMWorkerInfoSchema(_SchemaBase):
    sfm_module = fields.String(metadata={'nullable': False})
    sfm_function = fields.String(metadata={'nullable': False})
    name = fields.String(metadata={'nullable': False})
    description = fields.String(metadata={'nullable': False})


class _SFMWorkerRunsSchema(_SchemaBase):
    type = fields.Str(dump_default='runs')
    attributes = fields.Nested(_SFMWorkerRunsAttributesSchema)
    worker_config = fields.Nested(_SFMWorkerInfoSchema)


class _SFMWorkerIMessageSchema(_SchemaBase):
    """
    Schema implementation for serializing input messages for seismicity
    forecast model worker implementations.
    """
    data = fields.Nested(_SFMWorkerRunsSchema)


# Model Output
class SFMWorkerResponseDataAttributesSchema(_SchemaBase):
    """
    Schema representation of the SFM worker response data attributes.
    """
    status = fields.Str()
    status_code = fields.Int()
    forecast = fields.Nested(_ResultTimeBinSchema, many=True)
    warning = fields.Str(load_default='')
    model_result_uri = fields.String()

    @post_dump
    def clear_missing(self, data, **kwargs):
        return self._clear_missing(data)


# Model Output
class SFMWorkerResponseDataSchema(_SchemaBase):
    """
    Schema representation fo the SFM worker response data.
    """
    task_id = fields.UUID()
    attributes = fields.Nested(SFMWorkerResponseDataAttributesSchema)

    @post_dump
    def clear_missing(self, data, **kwargs):
        return self._clear_missing(data)


# Model Output
class SFMWorkerOMessageSchema(_SchemaBase):
    data = fields.Method("_serialize_data", deserialize="_deserialize_data")

    errors = fields.Dict()
    meta = fields.Dict()

    @post_dump
    def clear_missing(self, data, **kwargs):
        return self._clear_missing(data)

    def _serialize_data(self, obj):
        if 'data' in obj:
            if isinstance(obj['data'], list):
                return SFMWorkerResponseDataSchema(
                    many=True, context=self.context).dump(obj['data'])
            else:
                return SFMWorkerResponseDataSchema(
                    context=self.context).dump(obj['data'])

    def _deserialize_data(self, value):
        if isinstance(value, list):
            return SFMWorkerResponseDataSchema(
                context=self.context, many=True).load(value)

        return SFMWorkerResponseDataSchema(context=self.context).load(value)


class SFMWorkerIMessageSerializer(SerializerBase, IOBase):
    """
    Serializes a data structure which later can be consumed by SFM-Workers.
    """

    def __init__(self, transform=False, **kwargs):
        """
        :param transform: Whether to transform the data between crs's.
            This requires
            additional data to be passed in as kwargs
        """
        super().__init__(**kwargs)
        self.__ctx = kwargs
        if transform:
            self.set_coordinate_transformer(**kwargs)

    def _serialize(self, data):
        """
        Serializes a SFM-Worker payload from the ORM into JSON.
        """
        return _SFMWorkerIMessageSchema(context=self.__ctx).dumps(data)

    def _serialize_dict(self, data):
        return _SFMWorkerIMessageSchema(context=self.__ctx).dump(data)


class SFMWorkerOMessageDeserializer(DeserializerBase, IOBase):
    """
    Deserializes a data structure which later can be consumed by SFM workers.
    """

    def __init__(self, transform=False, **kwargs):
        """
        :param transform: Whether to transform the data between crs's.
            This requires
        additional data to be passed in as kwargs
        """
        super().__init__(**kwargs)
        self.__ctx = kwargs
        if transform:
            self.set_coordinate_transformer(**kwargs)
        self._many = kwargs.get('many', False)
        self._partial = kwargs.get('partial', True)

    def _deserialize(self, data):
        """
        Deserializes a data structure returned by SFM-Worker implementations.
        """
        return SFMWorkerOMessageSchema(
            context=self.__ctx, many=self._many).loads(data)

    def _loado(self, data):
        return SFMWorkerOMessageSchema(
            context=self.__ctx, many=self._many,
            partial=self._partial).load(data)


IOBase.register(SFMWorkerIMessageSerializer)
SerializerBase.register(SFMWorkerIMessageSerializer)
IOBase.register(SFMWorkerOMessageDeserializer)
DeserializerBase.register(SFMWorkerOMessageDeserializer)
