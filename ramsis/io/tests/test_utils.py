import unittest

from ramsis.io.utils import IOBase, CoordinateTransformer

WGS84_PROJ = "epsg:4326"
UTM_PROJ = "+proj=utm +zone=32 +ellps=WGS84 +datum=WGS84 +units=m +no_defs"
# These coordinates are equal (lat lon in wgs84, e,n in utm.)
LON = 7.0
LAT = 51.0

# 5595938.843164114 1019789.7913273775
EASTING = 349666.70366168075
NORTHING = 5631728.68267166
# Altitude doesnt get reprojected, expect it to stay the same.
ALTITUDE = 0.0

# reference point in UTM coordinates, center of local
# coordinate reference system
REF_EASTING = 10000
REF_NORTHING = 20000
REF_ALTITUDE = 100


class TransformationCoordsTestCase(unittest.TestCase):
    """
    Test case for checking back and forth transformation
    between source proj and local coordinate system.
    """

    def test_multiple_conversions(self):
        transformer = CoordinateTransformer(UTM_PROJ, REF_EASTING,
                                            REF_NORTHING, REF_ALTITUDE,
                                            WGS84_PROJ)
        lat = LAT
        lon = LON
        easting = EASTING
        northing = NORTHING
        altitude = ALTITUDE
        for i in range(10):
            easting, northing, altitude = transformer.\
                to_local_coords(
                    lon, lat, altitude)
            lon, lat, altitude = transformer.\
                from_local_coords(
                    easting, northing, altitude)
        self.assertAlmostEqual(lat, LAT)
        self.assertAlmostEqual(lon, LON)
        self.assertAlmostEqual(easting, EASTING)
        self.assertAlmostEqual(northing, NORTHING)
        self.assertAlmostEqual(altitude, ALTITUDE)

    def test_iobase(self):
        iobase_to_local = IOBase()
        iobase_to_local.set_coordinate_transformer(
            ref_easting=REF_EASTING, ref_northing=REF_NORTHING,
            ramsis_proj=UTM_PROJ, external_proj=WGS84_PROJ,
            transform_func_name='to_local_coords')
        iobase_to_external = IOBase()
        iobase_to_external.set_coordinate_transformer(
            ref_easting=REF_EASTING, ref_northing=REF_NORTHING,
            ramsis_proj=UTM_PROJ, external_proj=WGS84_PROJ,
            transform_func_name='from_local_coords')

        easting, northing, altitude = iobase_to_local.transform_func(
            LON, LAT, ALTITUDE)
        lon, lat, altitude = iobase_to_external.transform_func(
            EASTING, NORTHING, altitude)
        self.assertAlmostEqual(lat, LAT)
        self.assertAlmostEqual(lon, LON)
        self.assertAlmostEqual(easting, EASTING)
        self.assertAlmostEqual(northing, NORTHING)
