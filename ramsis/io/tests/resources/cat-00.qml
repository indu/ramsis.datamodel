<?xml version="1.0" encoding="UTF-8"?>
<q:quakeml xmlns="http://quakeml.org/xmlns/bed/1.2" xmlns:q="http://quakeml.org/xmlns/quakeml/1.2">
    <eventParameters publicID="smi:scs/0.7/EventParameters">
        <event publicID="smi:scs/0.7/knmi2011rqfm">
            <description>
                <text>Weeze</text>
                <type>nearest cities</type>
            </description>
            <description>
                <text>Germany</text>
                <type>region name</type>
            </description>
            <creationInfo>
                <agencyID>KNMI</agencyID>
                <creationTime>2015-05-06T12:41:48.671542Z</creationTime>
            </creationInfo>
            <magnitude publicID="smi:scs/0.7/NQ_Magnitude#20110908190251.010">
                <stationCount>67</stationCount>
                <creationInfo>
                    <agencyID>KNMI</agencyID>
                    <creationTime>2015-04-30T14:15:46.917924Z</creationTime>
                </creationInfo>
                <mag>
                    <value>4.5</value>
                    <uncertainty>0</uncertainty>
                </mag>
                <type>MLnq</type>
                <methodID>smi:scs/0.7/trimmed_mean</methodID>
            </magnitude>
            <origin publicID="smi:scs/0.7/NQ_Origin#20110908190251.010">
                <time>
                    <value>2011-09-08T19:02:51.01Z</value>
                    <uncertainty>0</uncertainty>
                </time>
                <longitude>
                    <value>6.211667</value>
                    <uncertainty>0</uncertainty>
                </longitude>
                <latitude>
                    <value>51.64017</value>
                    <uncertainty>0</uncertainty>
                </latitude>
                <quality>
                    <associatedPhaseCount>98</associatedPhaseCount>
                    <usedPhaseCount>98</usedPhaseCount>
                    <associatedStationCount>67</associatedStationCount>
                    <usedStationCount>67</usedStationCount>
                    <depthPhaseCount>0</depthPhaseCount>
                    <standardError>0.4</standardError>
                </quality>
                <evaluationMode>manual</evaluationMode>
                <creationInfo>
                    <agencyID>KNMI</agencyID>
                    <creationTime>2015-04-30T14:15:46.917924Z</creationTime>
                </creationInfo>
                <depth>
                    <value>10300</value>
                    <uncertainty>0</uncertainty>
                </depth>
                <methodID>smi:scs/0.7/Hypo91</methodID>
                <earthModelID>smi:scs/0.7/South</earthModelID>
                <evaluationStatus>final</evaluationStatus>
            </origin>
            <preferredOriginID>smi:scs/0.7/NQ_Origin#20110908190251.010</preferredOriginID>
            <preferredMagnitudeID>smi:scs/0.7/NQ_Magnitude#20110908190251.010</preferredMagnitudeID>
            <type>earthquake</type>
        </event>
        <event publicID="smi:scs/0.7/knmi2011ddji">
            <description>
                <text>Becheln</text>
                <type>nearest cities</type>
            </description>
            <description>
                <text>Germany</text>
                <type>region name</type>
            </description>
            <creationInfo>
                <agencyID>KNMI</agencyID>
                <creationTime>2015-05-06T12:40:05.082758Z</creationTime>
            </creationInfo>
            <magnitude publicID="smi:scs/0.7/NQ_Magnitude#20110214124312.980">
                <stationCount>23</stationCount>
                <creationInfo>
                    <agencyID>KNMI</agencyID>
                    <creationTime>2015-04-30T14:15:48.721258Z</creationTime>
                </creationInfo>
                <mag>
                    <value>4.4</value>
                    <uncertainty>0</uncertainty>
                </mag>
                <type>MLnq</type>
                <methodID>smi:scs/0.7/trimmed_mean</methodID>
            </magnitude>
            <origin publicID="smi:scs/0.7/NQ_Origin#20110214124312.980">
                <time>
                    <value>2011-02-14T12:43:12.98Z</value>
                    <uncertainty>0</uncertainty>
                </time>
                <longitude>
                    <value>7.731667</value>
                    <uncertainty>0</uncertainty>
                </longitude>
                <latitude>
                    <value>50.29</value>
                    <uncertainty>0</uncertainty>
                </latitude>
                <quality>
                    <associatedPhaseCount>37</associatedPhaseCount>
                    <usedPhaseCount>37</usedPhaseCount>
                    <associatedStationCount>23</associatedStationCount>
                    <usedStationCount>23</usedStationCount>
                    <depthPhaseCount>0</depthPhaseCount>
                    <standardError>0.41</standardError>
                </quality>
                <evaluationMode>manual</evaluationMode>
                <creationInfo>
                    <agencyID>KNMI</agencyID>
                    <creationTime>2015-04-30T14:15:48.721258Z</creationTime>
                </creationInfo>
                <depth>
                    <value>4100</value>
                    <uncertainty>0</uncertainty>
                </depth>
                <methodID>smi:scs/0.7/Hypo91</methodID>
                <earthModelID>smi:scs/0.7/South</earthModelID>
                <evaluationStatus>final</evaluationStatus>
            </origin>
            <preferredOriginID>smi:scs/0.7/NQ_Origin#20110214124312.980</preferredOriginID>
            <preferredMagnitudeID>smi:scs/0.7/NQ_Magnitude#20110214124312.980</preferredMagnitudeID>
            <type>earthquake</type>
        </event>
    </eventParameters>
</q:quakeml>