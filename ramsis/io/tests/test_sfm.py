# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
Testing facilities for SFM-Worker IO.
"""

import base64
import json
import os
import unittest
import uuid
from shapely import geometry

import dateutil
import dateutil.parser

from ramsis.datamodel.base import ORMBase  # noqa
from ramsis.datamodel.model import ( # noqa
    Tag, ModelConfig, ModelRun)
from ramsis.datamodel.seismicity import (  # noqa
    SeismicRate, ResultTimeBin,
    SeismicForecastGrid)
from ramsis.datamodel.seismics import (  # noqa
    SeismicForecastCatalog, SeismicEvent)
from ramsis.datamodel.type import JSONEncodedDict # noqa
from ramsis.datamodel.forecast import (  # noqa
    Forecast, ForecastSeries)
from ramsis.datamodel.project import Project, EInput  # noqa
from ramsis.io.sfm import (SFMWorkerIMessageSerializer,
                           SFMWorkerOMessageDeserializer)

RAMSIS_PROJ = ("+proj=utm +zone=32 +ellps=WGS84 +datum=WGS84 "
               "+units=m +x_0=0.0 +y_0=0.0 +no_defs")

LON = 10.663204912654692
LAT = 10.663205540045357

MIN_LON = 10.663204912654692
MAX_LON = 12.0
MIN_LAT = 10.663205540045357
MAX_LAT = 11.0


def _read(path):
    """
    Utility method reading testing resources from a file.
    """
    with open(path, 'rb') as ifd:
        retval = ifd.read()

    return retval.strip()


class SFMWorkerIMessageSerializerTestCase(unittest.TestCase):
    """
    Test for :py:class:`RAMSIS.io.sfm.SFMWorkerIMessageSerializer` class.
    """
    PATH_RESOURCES = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  'resources')
    maxDiff = None

    def test_dumps_imessage(self):
        geometry_extent_dict = {
            "type": "Polygon",
            "coordinates": [[[1.0, 0.0], [3.0, 0.0], [3.0, 2.0],
                             [1.0, 2.0], [1.0, 0.0]]]}
        geometry_wkt = geometry.shape(geometry_extent_dict).wkt

        reference_catalog = _read(os.path.join(self.PATH_RESOURCES,
                                               'cat-01.qml'))
        reference_catalog_b64 = base64.b64encode(
            reference_catalog).decode('utf-8')
        injection_well = [{
            'altitude': {'value': 400.0},
            'publicid': ('smi:ch.ethz.sed/bh/'
                         '11111111-e4a0-4692-bf29-33b5591eb798'),
            'sections': [{
                'toplongitude': {'value': LON},
                'toplatitude': {'value': LAT},
                'topaltitude': {'value': 1100.0},
                'topmeasureddepth': {'value': 0.0},
                'bottomlongitude': {'value': LON},
                'bottomlatitude': {'value': LAT},
                'bottomaltitude': {'value': 1000.0},
                'bottommeasureddepth': {'value': 100.0},
                'holediameter': {'value': 0.3},
                'topclosed': False,
                'bottomclosed': False,
                'publicid': ('smi:ch.ethz.sed/bh/section/'
                             '11111111-8d89-4f13-95e7-526ade73cc8b'),
                'hydraulics': [
                    {'datetime':
                        {'value': '2019-05-03T13:27:09.117623'}},
                    {'datetime':
                        {'value': '2019-05-03T15:27:09.117623'}}]}]}]
        injection_plan = {
            'altitude': {'value': 400.0},
            'publicid': ('smi:ch.ethz.sed/bh/'
                         '11111111-e4a0-4692-bf29-33b5591eb798'),
            'sections': [{
                'toplongitude': {'value': LON},
                'toplatitude': {'value': LAT},
                'topaltitude': {'value': 1100.0},
                'topmeasureddepth': {'value': 0.0},
                'bottomlongitude': {'value': LON},
                'bottomlatitude': {'value': LAT},
                'bottomaltitude': {'value': 1000.0},
                'bottommeasureddepth': {'value': 100.0},
                'holediameter': {'value': 0.3},
                'topclosed': False,
                'bottomclosed': False,
                'publicid': ('smi:ch.ethz.sed/bh/section/'
                         '11111111-8d89-4f13-95e7-526ade73cc8b'),
                'hydraulics': [
                    {'datetime':
                        {'value': '2019-05-03T17:27:09.117623'}},
                    {'datetime':
                        {'value': '2019-05-03T19:27:09.117623'}}]}]}

        attributes = {
            'seismic_catalog': reference_catalog_b64,
            'injection_well': injection_well,
            'injection_plan': injection_plan,
            'geometry': {'bounding_polygon': geometry_wkt,
                         'altitude_min': 0.0,
                         'altitude_max': 1.0},
            'model_parameters': {
                'a': 2,
                "model_module": "module",
                "model_class": "class",
                "model_name": "model_name",
                "model_description": "model description"}}

        reference_result = {
            'data': {
                'type': 'runs',
                'attributes': attributes}}

        serializer = SFMWorkerIMessageSerializer()

        payload = {
            'data': {
                'type': 'runs',
                'attributes': {
                    'seismic_catalog': reference_catalog,
                    'injection_well': json.dumps(
                        injection_well, ensure_ascii=False).encode('utf-8'),
                    'injection_plan': json.dumps(
                        injection_plan, ensure_ascii=False).encode('utf-8'),
                    'geometry': {'bounding_polygon': geometry_wkt,
                                 'altitude_min': 0.0,
                                 'altitude_max': 1.0},
                    'model_parameters': {
                        'a': 2,
                        "model_module": "module",
                        "model_class": "class",
                        "model_name": "model_name",
                        "model_description": "model description"}}}}
        dumped_result = serializer.dumps(payload)
        actual_result = json.loads(dumped_result)
        print(reference_result)
        print(actual_result)
        self.assertEqual(actual_result, reference_result)


class SFMWorkerOMessageDeserializerTestCase(unittest.TestCase):
    """
    Test for :py:class:`RAMSIS.io.sfm.SFMWorkerOMessageDeserializer` class.
    """

    PATH_RESOURCES = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  'resources')

    def test_error_msg(self):
        json_omsg = _read(os.path.join(self.PATH_RESOURCES,
                                       'omsg-500.json'))

        reference_result = {
            'data': {
                'task_id': uuid.UUID('491a85d6-04b3-4528-8422-acb348f5955b'),
                'attributes': {
                    'status_code': 500,
                    'status': 'WorkerError',
                    'warning': 'Caught in default model exception handler.'}}}

        deserializer = SFMWorkerOMessageDeserializer()

        self.assertEqual(deserializer.loads(json_omsg),
                         reference_result)

    def test_accepted_msg(self):
        json_omsg = _read(os.path.join(self.PATH_RESOURCES,
                                       'omsg-202.json'))

        reference_result = {
            'data': {
                'task_id': uuid.UUID('f51e0208-515d-4099-8d87-bdc0e54f09cb'),
                'attributes': {
                    'status_code': 202,
                    'status': 'TaskAccepted',
                    'warning': '', }}}

        deserializer = SFMWorkerOMessageDeserializer()

        self.assertEqual(deserializer.loads(json_omsg),
                         reference_result)

    def test_ok_msg_single_value(self):
        json_omsg = _read(os.path.join(self.PATH_RESOURCES,
                                       'omsg-200.json'))

        r0 = SeismicRate(
            latitude_min=MIN_LAT,
            longitude_min=MIN_LON,
            altitude_min=0.0,
            latitude_max=MAX_LAT,
            longitude_max=MAX_LON,
            altitude_max=0.0,
            b_value=73,
            a_value=1.,
            mc_value=1.5,
            numberevents_value=42.0)

        g0 = SeismicForecastGrid(
            seismicrates=[r0])

        t0 = ResultTimeBin(
            starttime=dateutil.parser.parse(
                '2019-07-02T14:59:52.508142'),
            endtime=dateutil.parser.parse('2019-07-02T14:59:52.508142'),
            seismicforecastgrids=[g0, ])

        reference_result = {
            'data': {
                'task_id': uuid.UUID('491a85d6-04b3-4528-8422-acb348f5955b'),
                'attributes': {
                    'status_code': 200,
                    'status': 'TaskCompleted',
                    'warning': '',
                    'forecast': [t0, ]}}}

        deserializer = SFMWorkerOMessageDeserializer()
        omsg = deserializer.loads(json_omsg)
        self.assertEqual(omsg,
                         reference_result)


def suite():
    suite = unittest.TestSuite()
    suite.addTest(
        unittest.makeSuite(SFMWorkerIMessageSerializerTestCase, 'test'))
    suite.addTest(
        unittest.makeSuite(SFMWorkerOMessageDeserializerTestCase, 'test'))
    return suite


if __name__ == '__main__':
    unittest.main(defaultTest='suite')
