from os.path import join, splitext
import json
from sqlalchemy import select
from shapely import geometry
from marshmallow import fields, Schema, post_load, post_dump
from ramsis.datamodel import Tag, ModelConfig, ForecastSeries, \
    EInput, Project, InjectionPlan, EResultType
from ramsis.io.utils import DateTimeShort, CoordinateTransformer


class TagField(fields.Field):
    """Field that serializes to a string of numbers and deserializes
    to a list of numbers.
    """
    def _serialize(self, value, attr, obj, **kwargs):
        return value.name

    def _deserialize(self, value, attr, data, **kwargs):
        session = self.context.get('session')
        existing_tag = session.execute(
            select(Tag).filter_by(name=value)).scalar()
        if existing_tag:
            return existing_tag
        else:
            return Tag(name=value)


class ModelConfigurationSchema(Schema):
    name = fields.Str()
    url = fields.Str()
    sfm_module = fields.Str()
    sfm_function = fields.Str()
    result_type = fields.Enum(EResultType, required=True)
    description = fields.Str()
    enabled = fields.Bool()
    config = fields.Dict()
    tags = fields.List(TagField(), required=True)

    @post_load
    def create_model_config(self, data, **kwargs):
        return ModelConfig(**data)


class ProjectConfigurationSchema(Schema):
    name = fields.Str(required=True)
    starttime = DateTimeShort(required=True)
    endtime = DateTimeShort(required=False)
    description = fields.Str()
    hydws_url = fields.Str()
    fdsnws_url = fields.Str()
    seismiccatalog_required = fields.Enum(EInput, required=True)
    injectionwell_required = fields.Enum(EInput, required=True)
    injectionplan_required = fields.Enum(EInput, required=True)

    @post_load
    def create_input_data(self, data, **kwargs):
        return Project(**data)

    @post_dump
    def remove_data(self, data, **kwargs):
        if data["injectionwell"]:
            data["injectionwell"] = "Injection well exists"

        if data["seismiccatalog"]:
            data["seismiccatalog"] = "seismic catalog exists"

        return data


class ForecastSeriesConfigurationSchema(Schema):
    name = fields.Str(required=True)
    starttime = DateTimeShort(required=True)
    endtime = DateTimeShort(required=False)
    forecastinterval = fields.Int(required=False)
    forecastduration = fields.Int(required=False)
    tags = fields.List(TagField(), required=True)
    altitudemin = fields.Float(required=False)
    altitudemax = fields.Float(required=False)
    boundingpolygon = fields.Str(required=False)
    boundingbox = fields.Dict()
    injectionplan_dir = fields.Str()
    injectionplan_filenames = fields.List(fields.Str())

    @post_load
    def add_data(self, data, **kwargs):
        if "boundingpolygon" in data.keys():
            pass

        elif "boundingbox" in data.keys():
            if data["boundingbox"]:
                bb_data = data["boundingbox"]
                bbox = (bb_data["x_min"], bb_data["y_min"],
                        bb_data["x_max"], bb_data["y_max"])
                polygon = geometry.box(*bbox, ccw=True)
                if "srid" in bb_data.keys():
                    if bb_data["srid"]:
                        transformer = CoordinateTransformer(
                            bb_data["srid"])
                        polygon = transformer.polygon_from_local_coords(
                            polygon)
                data['boundingpolygon'] = polygon.wkt
            del data["boundingbox"]
        assert data["boundingpolygon"] is not None

        if 'injectionplan_dir' in data.keys():
            if data['injectionplan_dir']:
                injection_plans = []
                for fname in data['injectionplan_filenames']:
                    fname_truncated = splitext(fname)[0]
                    with open(join(data["injectionplan_dir"], fname), "r") \
                            as ofile:
                        plan_data = json.dumps(
                            json.loads(ofile.read()),
                            ensure_ascii=False).encode('utf-8')
                        injection_plans.append(
                            InjectionPlan(name=fname_truncated,
                                          data=plan_data))
            data["injectionplans"] = injection_plans

            del data['injectionplan_dir']
            del data['injectionplan_filenames']
        return ForecastSeries(**data)


class MasterConfigurationSchema(Schema):
    project_config = fields.Str(required=True)
    model_config = fields.Str(required=True)
    forecastseries_config = fields.Str()
    catalog = fields.Str()
    wells = fields.Str()
