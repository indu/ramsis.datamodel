# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
General purpose IO utilities.
"""

import abc
import enum
import functools
import logging
from functools import partial

import numpy as np
from pyproj import Transformer
from shapely.ops import transform
from shapely import geometry
from shapely.geometry import Polygon
from shapely.affinity import translate
from typing import Union, Tuple


from marshmallow import fields, validate, Schema


class _PolygonSchema(Schema):
    type = fields.Str(required=True)
    coordinates = fields.List(fields.List(fields.Float), required=True)


def validate_positive(d):
    return d >= 0


def validate_altitude(d):
    return d >= -7000. and d <= 8000.


validate_longitude = validate.Range(min=-180., max=180.)
validate_latitude = validate.Range(min=-90., max=90)
validate_ph = validate.Range(min=0, max=14)

Latitude = partial(fields.Float, validate=validate_latitude)
RequiredLatitude = partial(Latitude, required=True)
Longitude = partial(fields.Float, validate=validate_longitude)
RequiredLongitude = partial(Longitude, required=True)

validate_percentage = validate.Range(min=0, max=100)

DateTime = functools.partial(fields.DateTime, format='%Y-%m-%dT%H:%M:%S.%f')
DateTimeShort = functools.partial(fields.DateTime, format='%Y-%m-%dT%H:%M:%S')
Percentage = functools.partial(fields.Float, validate=validate_percentage)
Positive = functools.partial(fields.Float, validate=validate_positive)
Altitude = functools.partial(fields.Float, validate=validate_altitude)
Uncertainty = Positive


class ExitCode(enum.Enum):
    EXIT_SUCCESS = 0
    EXIT_WARNING = 1
    EXIT_ERROR = 2


class Error(Exception):
    """Error base class"""
    exit_code = ExitCode.EXIT_ERROR.value
    traceback = False

    def __init__(self, *args):
        super().__init__(*args)
        self.args = args

    def get_message(self):
        return type(self).__doc__.format(*self.args)

    __str__ = get_message


class ErrorWithTraceback(Error):
    """Error with traceback."""
    traceback = True


class _IOError(Error):
    """Base IO error ({})."""


class InvalidProj4(_IOError):
    """Invalid Proj4 projection specified: {}"""


class TransformationError(_IOError):
    """Error while performing CRS transformation: {}"""


class NoOriginError(_IOError):
    """Error while performing CRS transformation: {}"""


class NoMagnitudeError(_IOError):
    """Error while performing CRS transformation: {}"""
# -----------------------------------------------------------------------------


class IOBase(abc.ABC):
    """
    Abstract IO base class
    """
    LOGGER = 'ramsis.io.io'

    def __init__(self, **kwargs):
        self.logger = logging.getLogger(self.LOGGER)
        self.coordinate_transformer = None
        self.transform_func = None

    def set_coordinate_transformer(self, **kwargs):
        self.external_proj = kwargs.get("external_proj")
        if not self.external_proj:
            self.external_proj = 'epsg:4326'

        self.altitude = kwargs.get("altitude")
        if not self.altitude:
            self.altitude = 0.0

        for atr in ['ref_easting', 'ref_northing', 'ramsis_proj',
                    'transform_func_name']:
            if kwargs.get(atr) is None:
                raise ValueError(
                    f"IOBase requires {atr} to be passed in kwargs.")
            setattr(self, atr, kwargs.get(atr))

        self.coordinate_transformer = CoordinateTransformer(
            self.ramsis_proj, self.ref_easting, self.ref_northing,
            self.altitude, self.external_proj)
        self.transform_func = _callable_or_raise(
            getattr(self.coordinate_transformer, self.transform_func_name))

    def transform_callback(self, func):
        """
        Decorator that registers a custom SRS transformation.

        The function should receive the coordinates :code:`x`, :code:`y,
        :code:`z`, and an optional projection. The function is required to
        return a tuple of the transformed values.  Overrides the deserializer's
        :code:`_transform` method.

        :param callable func: The SRS transformation to be registered

        The usage is illustrated bellow:

        .. code::

            deserializer = QuakeMLDeserializer(proj=proj)

            @deserializer.transform_callback
            def crs_transform(x, y, z, proj):
                return pymap3d_transform(x, y, z, proj)

            cat = deserializer.load(data)
        """
        self._transform_callback = _callable_or_raise(
            getattr(self.coordinate_transformer, func))
        return func


class SerializerBase(abc.ABC):
    """
    Abstract base class for serializers.
    """

    @abc.abstractmethod
    def _serialize(self, data):
        pass

    def dumps(self, data):
        """
        Serialize to string. Alias for :py:meth:`_serialize`.

        :param data: Data to be serialized.
        """
        return self._serialize(data)

    def _dumpo(self, data):
        """
        Serialize to :code:`dict`.

        :param data: Data to be serialized.

        :rtype: dict
        """
        raise NotImplementedError


class DeserializerBase(abc.ABC):
    """
    Abstract base class for deserializers.
    """

    @abc.abstractmethod
    def _deserialize(self):
        pass

    def load(self, ifd):
        """
        Deserialize data from a file-like object (i.e. must support
        :code:`.read()`).
        """
        return self._deserialize(ifd.read())

    def loads(self, data, **kwargs):
        """
        Deserialize :code:`data` from string.

        :param data: Data to be deserialized.
        :type data: str or bytes
        """
        return self._deserialize(data, **kwargs)

    def _loado(self, data, **kwargs):
        """
        Deserialize :code:`data` from an object.

        :param data: Data to be deserialized.
        """
        raise NotImplementedError


# -----------------------------------------------------------------------------
def append_ms_zeroes(dt_str):
    """
    Utility function zero padding milliseconds.

    :param str dt_str: Date time string to be processed

    :returns: Date time string with padded milliseconds
    :rtype: str
    """
    if '.' in dt_str:
        return dt_str
    return dt_str + '.000000'


def _callable_or_raise(obj):
    """
    Makes sure an object is callable if it is not :code:`None`.

    :returns: Object validated
    :raises: code:`ValueError` if :code:`obj` is not callable
    """
    if obj and not callable(obj):
        raise ValueError(f"{obj!r} is not callable.")
    return obj


class CoordinateTransformer:
    """
    Class to transform between a external geographic (default ESPG:4326,
    also known as WGS84), and a local cartesian CRS.

    Any EPSG code or proj4 string can be used for the local_proj input,
    for instance 2056 to represent the swiss coordinate system, or
    "+proj=utm +zone=32 +ellps=WGS84 +datum=WGS84 +units=m +no_defs"
    to represent a UTM coordinate system.
    """

    def __init__(
            self,
            local_proj: Union[int, str],
            ref_easting: float = 0.0,
            ref_northing: float = 0.0,
            ref_altitude: float = 0.0,
            external_proj: Union[int, str] = 4326):
        """
        Constructor of CoordinateTransformer object.

        :param local_proj: int (epsg) or string (proj) of local CRS.
        :param ref_easting: reference easting for local coordinates.
        :param ref_northing: reference northing for local coordinates.
        :param ref_altitude: reference altitude for local coordinates.
        :param external_proj: int or string of geographic coordinates.
        """
        self.ref_easting = ref_easting
        self.ref_northing = ref_northing
        self.ref_altitude = ref_altitude
        self.local_proj = local_proj
        self.external_proj = external_proj

        self.transformer_to_local = Transformer.from_proj(
            self.external_proj, self.local_proj, always_xy=True)
        self.transformer_to_external = Transformer.from_proj(
            self.local_proj, self.external_proj, always_xy=True)

    def to_local_coords(self,
                        lon: Union[float, list],
                        lat: Union[float, list],
                        altitude: Union[float, list] = None):
        """
        Transform geographic coordinates to local coordinates.

        :param lon: longitude
        :param lat: latitude
        :param altitude: altitude
        :returns: Easting, northing and altitude in local CRS relative to ref.
        """
        enu = \
            self.transformer_to_local.transform(lon, lat, altitude)
        print("enu: ", enu[0], self.ref_altitude)
        easting = np.array(enu[0]) - self.ref_easting
        northing = np.array(enu[1]) - self.ref_northing

        if altitude is not None:
            new_altitude = np.array(enu[2]) - self.ref_altitude
        else:
            new_altitude = None

        return easting, northing, new_altitude

    def from_local_coords(
            self,
            easting: Union[float, list],
            northing: Union[float, list],
            altitude: Union[float, list] = None):
        """
        Transform local coordinates to geographic coordinates.

        :param easting: easting
        :param northing: northing
        :param altitude: altitude
        :returns: longitude, latitude, altitude in local CRS relative to ref.
        """
        easting_0 = np.array(easting) + self.ref_easting
        northing_0 = np.array(northing) + self.ref_northing

        if altitude is not None:
            new_altitude = np.array(altitude) + self.ref_altitude
        else:
            new_altitude = None

        enu = self.transformer_to_external.transform(easting_0,
                                                     northing_0,
                                                     new_altitude)
        if new_altitude is None:
            enu = (enu[0], enu[1], None)

        return enu

    def polygon_to_local_coords(self, polygon: Polygon) -> Polygon:
        """
        Transform polygon to local coordinates.
        """
        new_polygon = transform(
            self.transformer_to_local.transform, polygon)
        translated_polygon = translate(
            new_polygon, xoff=-self.ref_easting,
            yoff=-self.ref_northing, zoff=-self.ref_altitude)
        return translated_polygon

    def polygon_from_local_coords(self, polygon: Polygon) -> Polygon:
        """
        Transform polygon to local coordinates.
        """
        translated_polygon = translate(
            polygon, xoff=self.ref_easting,
            yoff=self.ref_northing, zoff=self.ref_altitude)
        new_polygon = transform(
            self.transformer_to_external.transform, translated_polygon)
        return new_polygon


def bounding_box_to_polygon(x_min, x_max, y_min, y_max, srid=None) -> Polygon:
    bbox = (x_min, y_min,
            x_max, y_max)
    return geometry.box(*bbox, ccw=True)


def polygon_to_bounding_box(polygon: Polygon) -> Tuple[int, int, int, int]:
    (minx, miny, maxx, maxy) = polygon.bounds
    return (minx, miny, maxx, maxy)
