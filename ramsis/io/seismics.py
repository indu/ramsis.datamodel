# Copyright 2019, ETH Zurich - Swiss Seismological Service SED
"""
Utilities for seismic data import.
"""
import datetime
import io

from lxml import etree
from obspy import read_events
from marshmallow import (Schema, fields, post_dump, post_load,
                         pre_dump)
import base64

from ramsis.datamodel.seismics import SeismicEvent, \
    SeismicForecastCatalog
from ramsis.io.utils import (DeserializerBase, IOBase, SerializerBase,
                             _IOError, NoOriginError)

_QUAKEML_HEADER = (
    b'<?xml version="1.0" encoding="UTF-8"?>'
    b'<q:quakeml xmlns="http://quakeml.org/xmlns/bed/1.2" '
    b'xmlns:q="http://quakeml.org/xmlns/quakeml/1.2">'
    b'<eventParameters publicID="smi:scs/0.7/EventParameters">')

_QUAKEML_FOOTER = b'</eventParameters></q:quakeml>'


class QuakeMLCatalogIOError(_IOError):
    """QuakeML de-/serialization error ({})."""


class InvalidMagnitudeType(QuakeMLCatalogIOError):
    """Event with invalid magnitude type {!r} detected."""


class _SchemaBase(Schema):
    """
    Schema base class.
    """
    class Meta:
        ordered = True
        strict = True


class _SeismicForecastCatalogSchema(_SchemaBase):
    """
    Schema representation for a Seismic Forecast Catalog.
    This loads from base64 encoded catalog to a SeismicForecastCatalog
    db object.
    """

    quakeml = fields.String(required=False)

    @pre_dump
    def make_catalog(self, data, **kwargs):
        """
        Convert an instance of
        :py:class:`ramsis.datamodel.seismics.SeismicCatalog`
        into its `QuakeML <https://quake.ethz.ch/quakeml/>`_ representation.
        """
        retval = dict()
        if getattr(data, 'events'):
            retval['quakeml'] = QuakeMLForecastCatalogSerializer().dumps(data)

        return retval

    @post_dump
    def b64encode(self, data, **kwargs):
        """
        Encode the catalog using base64 encoding.
        """
        if 'quakeml' in data:
            data['quakeml'] = base64.b64encode(
                data['quakeml'].encode('utf8')).decode('utf8')

        return data

    @post_load
    def make_object(self, data, **kwargs):
        if 'quakeml' in data:

            data['quakeml'] = base64.b64decode(
                data['quakeml'].encode('utf8')).decode('utf8')

            retval = QuakeMLForecastCatalogDeserializer().loads(
                data.pop('quakeml'), **data)
        else:
            retval = SeismicForecastCatalog(
                creationinfo_creationtime=datetime.datetime.utcnow())
        return retval


class QuakeMLForecastCatalogDeserializer(DeserializerBase, IOBase):
    """
    Deserializes `QuakeML <https://quake.ethz.ch/quakeml/>`_ data into an
    RT-RAMSIS seismic catalog.
    """
    NSMAP_QUAKEML = {None: "http://quakeml.org/xmlns/bed/1.2",
                     'q': "http://quakeml.org/xmlns/quakeml/1.2"}
    LOGGER = 'RAMSIS.io.quakemldeserializer'

    def __init__(self, transform=False, mag_type=None, **kwargs):
        """
        :param transform: Whether to transform the data between crs's.
        This requires additional data to be passed in as kwargs
        :param mag_type: Describes the type of magnitude events should be
            configured with. If :code:`None` the magnitude type is not
            verified (default).
        :type mag_type: str or None
        :param transform_func: Function reference for transforming data
            into local coordinate system
        """
        super().__init__(**kwargs)
        self.__ctx = kwargs
        if transform:
            self.set_coordinate_transformer(**kwargs)

        self._mag_type = mag_type

    def _deserialize_event(self, event_element, **kwargs):
        """
        Deserialize a single `QuakeML <https://quake.ethz.ch/quakeml/>`_
        event and perform *indexing*.

        :param bytes event_element: `QuakeML <https://quake.ethz.ch/quakeml/>`_
            event element

        :returns: Seismic event
        :rtype: :py:class:`ramsis.datamodel.seismics.Event`
        """
        def create_pseudo_catalog(event_element):
            """
            Creates a QuakeML catalog from a single event.

            :param bytes event_element: `QuakeML
                <https://quake.ethz.ch/quakeml/>`_ event element

            .. note::

                ObsPy does not implement parsing *plain* QuakeML events. As a
                consequence, this workaround is in use i.e. creating a pseudo
                catalog from a single event.
            """
            return io.BytesIO(
                _QUAKEML_HEADER + event_element + _QUAKEML_FOOTER)

        def add_prefix(prefix, d, replace_args=['_', '']):
            if not replace_args:
                replace_args = ["", ""]

            return {"{}{}".format(prefix, k.replace(*replace_args)): v
                    for k, v in d.items()}

        try:
            e = read_events(create_pseudo_catalog(event_element))[0]
        except Exception as err:
            raise QuakeMLCatalogIOError(f'While parsing QuakeML: {err}')
        else:
            self.logger.info(f"Importing seismic event: {e.short_str()} ...")

        attr_dict = {}
        magnitude = e.preferred_magnitude()
        attr_dict['magnitude_value'] = magnitude.mag
        attr_dict.update(add_prefix('magnitude_', magnitude.mag_errors))

        origin = e.preferred_origin()
        attr_dict['datetime_value'] = origin.time.datetime
        attr_dict.update(add_prefix('datetime_', origin.time_errors))

        attr_dict['longitude_value'] = origin.longitude
        attr_dict.update(add_prefix('longitude_', origin.longitude_errors))
        attr_dict['latitude_value'] = origin.latitude
        attr_dict.update(add_prefix('latitude_', origin.latitude_errors))
        # take depth value as negative altitude - although the real meaning
        # usually depends on the catalog....
        attr_dict.update(add_prefix('altitude_', origin.depth_errors))
        if origin.depth is not None:
            attr_dict['altitude_value'] = -origin.depth
        else:
            raise NoOriginError("no depth on origin for event "
                                f"{event_element}")
        if origin.depth_errors:
            attr_dict.update(add_prefix('altitude_', origin.depth_errors))

        attr_dict['quakeml'] = event_element

        return SeismicEvent(**attr_dict)

    def _get_events(self, data, parser=etree.iterparse):
        """
        Generator yielding the deserialized events of a seismic catalog.

        :param data: Data events are deserialized from
        """
        parse_kwargs = {'events': ('end',),
                        'tag': "{%s}event" % self.NSMAP_QUAKEML[None]}

        try:
            for event, element in parser(data, **parse_kwargs):
                if event == 'end' and len(element):
                    yield self._deserialize_event(etree.tostring(element))

        except etree.XMLSyntaxError as err:
            raise QuakeMLCatalogIOError(err)

    def _deserialize(self, data, **kwargs):
        """
        Deserialize `QuakeML <https://quake.ethz.ch/quakeml/>`_ data into an
        RT-RAMSIS seismic catalog.

        :param data: Data to be deserialized.

        :returns: Seismic catalog
        :rtype: :py:class:`ramsis.datamodel.seismics.SeismicForecastCatalog`
        """
        if isinstance(data, str):
            data = data.encode(encoding='utf-8')

        if not isinstance(data, (bytes, bytearray)):
            raise TypeError('The data object must be str, bytes or bytearray, '
                            'not {!r}'.format(data.__class__.__name__))

        return SeismicForecastCatalog(
            creationinfo_creationtime=datetime.datetime.utcnow(),
            events=[e for e in self._get_events(io.BytesIO(data))], **kwargs)

    def _loado(self, data, **kwargs):
        return SeismicForecastCatalog(
            creationinfo_creationtime=datetime.datetime.utcnow(),
            events=[e for e in self._get_events(data, parser=etree.iterwalk)],
            **kwargs)


class QuakeMLForecastCatalogSerializer(SerializerBase, IOBase):
    """
    Serializes a RT-RAMSIS seismic catalog into `QuakeML
    <https://quake.ethz.ch/quakeml/>`_.
    """

    def __init__(self, transform=False, **kwargs):
        """
        :param transform: Whether to transform the data between crs's.
        This requires additional data to be passed in as kwargs
        """
        super().__init__(**kwargs)
        self.__ctx = kwargs
        if transform:
            self.set_coordinate_transformer(**kwargs)

    def _serialize(self, data):
        """
        Serialize a seismic forecast catalog.

        :param data: Seismic catalog
        :type data: :py:class:
            `ramsis.datamodel.seismics.SeismicObservationCatalog`

        :returns: Serialized QuakeML seismic catalog
        :rtype: str
        """
        return (_QUAKEML_HEADER + b''.join(e.quakeml for e in data.events)
                + _QUAKEML_FOOTER).decode('utf-8')


IOBase.register(QuakeMLForecastCatalogDeserializer)
DeserializerBase.register(QuakeMLForecastCatalogDeserializer)
SerializerBase.register(QuakeMLForecastCatalogSerializer)
