import json
from datetime import datetime

from marshmallow import EXCLUDE, Schema, fields, post_load, pre_load
from pandas import Timestamp
from seismostats.seismicity.catalog import ForecastCatalog
from seismostats.seismicity.rategrid import ForecastGRRateGrid

from ramsis.datamodel import (ResultTimeBin, SeismicForecastCatalog,
                              SeismicForecastGrid, SeismicRate)
from ramsis.io.seismics import (_QUAKEML_FOOTER, _QUAKEML_HEADER,
                                QuakeMLForecastCatalogDeserializer)
from ramsis.io.utils import (DeserializerBase, Percentage, RequiredLatitude,
                             RequiredLongitude, Uncertainty)


class DateTimeField(fields.DateTime):
    def _deserialize(self, value, attr, data):
        if isinstance(value, Timestamp):
            return value.to_pydatetime()
        elif isinstance(value, datetime):
            return value
        return super()._deserialize(value, attr, data)


class UserSchema(Schema):
    class Meta:
        unknown = EXCLUDE


class _SFMSeismicRateSchema(UserSchema):
    latitude_min = RequiredLatitude()
    latitude_max = RequiredLatitude()
    longitude_min = RequiredLongitude()
    longitude_max = RequiredLongitude()
    altitude_min = fields.Float()
    altitude_max = fields.Float()

    numberevents_value = fields.Float(data_key='number_events')
    numberevents_uncertainty = Uncertainty()
    numberevents_loweruncertainty = Uncertainty()
    numberevents_upperuncertainty = Uncertainty()
    numberevents_confidencelevel = Percentage()

    b_value = fields.Float(data_key='b')
    b_uncertainy = Uncertainty()
    b_loweruncertainty = Uncertainty()
    b_upperuncertainty = Uncertainty()
    b_confidencelevel = Percentage()

    a_value = fields.Float(data_key='a')
    a_uncertainy = Uncertainty()
    a_loweruncertainty = Uncertainty()
    a_upperuncertainty = Uncertainty()
    a_confidencelevel = Percentage()

    alpha_value = fields.Float(data_key='alpha')
    alpha_uncertainy = Uncertainty()
    alpha_loweruncertainty = Uncertainty()
    alpha_upperuncertainty = Uncertainty()
    alpha_confidencelevel = Percentage()

    mc_value = fields.Float(data_key='mc')
    mc_uncertainy = Uncertainty()
    mc_loweruncertainty = Uncertainty()
    mc_upperuncertainty = Uncertainty()
    mc_confidencelevel = Percentage()

    @pre_load
    def preload(self, data, **kwargs):
        # I don't know what depth relates to here.
        data['altitude_min'] = -data['depth_min']
        data['altitude_max'] = -data['depth_max']
        return data

    @post_load
    def make_object(self, data, **kwargs):
        return SeismicRate(**data)


class _SFMSeismicForecastGridSchema(UserSchema):
    seismicrates = fields.Nested(_SFMSeismicRateSchema(many=True))

    @pre_load
    def preload(self, data, **kwargs):
        # here, data is going to be one rate per grid,
        # as there are not multiple cells. If there were
        # multiple cells, would need to group this at the level above

        retval = dict(seismicrates=data)
        return retval

    @post_load
    def make_object(self, data, **kwargs):
        return SeismicForecastGrid(**data)


class _SFMGRResultTimeBinSchema(UserSchema):
    starttime = DateTimeField(required=True)
    endtime = DateTimeField(required=True)
    seismicforecastgrids = fields.Nested(
        _SFMSeismicForecastGridSchema(many=True))

    @pre_load
    def preload(self, data, **kwargs):
        rows = json.loads(
            data.to_json(orient='records'))
        grids = dict()
        for r in rows:
            if r['grid_id'] not in grids.keys():
                grids[r['grid_id']] = [r]
            else:
                grids[r['grid_id']].append(r)

        rates_per_grid_id = [grids[i] for i in grids.keys()]

        retval = dict(
            starttime=data.starttime,
            endtime=data.endtime,
            seismicforecastgrids=rates_per_grid_id)
        return retval

    @post_load
    def make_object(self, data, **kwargs):
        return ResultTimeBin(**data)


class _SFMSeismicEventSchema(UserSchema):
    latitude_min = RequiredLatitude()
    latitude_max = RequiredLatitude()
    longitude_min = RequiredLongitude()
    longitude_max = RequiredLongitude()
    altitude_min = fields.Float()
    altitude_max = fields.Float()

    b_value = fields.Float(data_key='b')
    b_uncertainy = Uncertainty()
    b_loweruncertainty = Uncertainty()
    b_upperuncertainty = Uncertainty()
    b_confidencelevel = Percentage()


class _SFMSeismicForecastCatalogSchema(UserSchema):
    quakeml = fields.String(required=False)

    @post_load
    def make_object(self, data, **kwargs):
        if 'quakeml' in data:

            retval = QuakeMLForecastCatalogDeserializer().loads(
                data.pop('quakeml'), **data)
        else:
            retval = SeismicForecastCatalog(
                creationinfo_creationtime=datetime.datetime.utcnow())
        return retval


class _SFMCatResultTimeBinSchema(UserSchema):
    starttime = DateTimeField(required=True)
    endtime = DateTimeField(required=True)
    seismicforecastcatalogs = fields.Nested(
        _SFMSeismicForecastCatalogSchema(many=True))

    @pre_load
    def preload(self, data: ForecastCatalog, **kwargs):

        seismicforecastcatalogs = [dict(quakeml=cat)
                                   for cat in data.to_quakeml()]

        empty_catalogs = [
            dict(quakeml=(_QUAKEML_HEADER + _QUAKEML_FOOTER).decode('utf-8'))
            for cat in range(data.empty_catalogs)]
        seismicforecastcatalogs.extend(empty_catalogs)

        retval = dict(
            starttime=data.starttime,
            endtime=data.endtime,
            seismicforecastcatalogs=seismicforecastcatalogs)
        return retval

    @post_load
    def make_object(self, data, **kwargs):
        return ResultTimeBin(**data)


class SFMResultDeserializer:
    """
    Deserializes a list of dataframe style objects that have been output
    by the seismicity forecast model, which can be consumed
    by the datamodel to be saved in a database.
    """

    def _deserialize(self, data, **kwargs):
        """
        Deserializes a data structure returned by SFM-Worker implementations.
        """
        assert isinstance(data, list)
        if isinstance(data[0], ForecastGRRateGrid):
            return _SFMGRResultTimeBinSchema(many=True).load(data)
        elif isinstance(data[0], ForecastCatalog):
            try:
                return _SFMCatResultTimeBinSchema(many=True).load(data)
            except KeyError:
                raise Exception('starttime and endtime must be included '
                                'in kwargs for catalog results')


DeserializerBase.register(SFMResultDeserializer)
