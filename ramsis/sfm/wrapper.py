import logging
from abc import ABC, abstractmethod

logger = logging.getLogger(__name__)


class SFM(ABC):
    def __new__(cls, *args, from_method=False, **kwargs):
        """
        Classes inheriting from this abstract class should only be instantiated
        using a constructor method. This is implemented by overwriting the
        __new__ method.
        """
        # create object
        obj = super(SFM, cls).__new__(cls)
        obj._from_method = from_method
        # call super init function
        super(cls, obj).__init__(*args, **kwargs)
        return obj

    @abstractmethod
    def __init__(self, *args, **kwargs):
        # set up logger
        self._logger = logging.getLogger(__name__)

        # assert that object has been created using the method constructor.
        if not hasattr(self, '_from_method') or not self._from_method:
            name = self.__class__.__name__
            raise Exception(
                f'The class "{name}" needs to be instantiated using the class'
                f' method "{name}.initialize()".')

    @classmethod
    def factory(cls, *args, **kwargs):
        return cls.__new__(cls, *args, from_method=True, **kwargs)

    @classmethod
    @abstractmethod
    def initialize(cls):
        """
        Factory method to initialize a new instance. Runs all the setup actions
        and transformations needed to provide the input for the models.

        Models can only be instantiated using this method. Needs to create a
        new instance using the SFM.factory(*args, **kwargs**) method.
        """
        pass

    @abstractmethod
    def parse_results(self, results):
        """
        This method is responsible of parsing the results into the correct
        output format.

        :param results: The results (return values) of the model.

        :return: A list of ResultTimeBin objects.
        """

    @abstractmethod
    def run(self):
        """
        Run the model. Runs all the necessary methods and parses them using
        self.parse_results.
        """
