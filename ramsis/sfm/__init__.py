"""Initialise standard set of classes and methods for
running stand alone models and also provides entry point for
model worker web services.
"""
import importlib

from ramsis.sfm.wrapper import SFM
from ramsis.datamodel.seismicity import ResultTimeBin


def get_model(model_module: str, model_class: str) -> SFM:
    """Defines what model class to import, e.g.
        from {model_module} import {model_class}

    :param model_module:    name of module containing the model_function
                            e.g. 'ramsis.sfm.em1.core.em1_model'
    :param model_class:     Str class name that is
                            an entry point within the module called. This
                            function must specify all the expected args
                            defined in the runner, and any additional model
                            specific arguments.
    """
    try:
        module = importlib.import_module(model_module)
        Model = getattr(module, model_class)
    except (ImportError, AttributeError):
        raise ValueError(
            f"Unknown import module: {model_module}, model: {model_class}")

    return Model


def model_runner(model: SFM,
                 data: dict) -> ResultTimeBin:
    """
    Runs a model from the models folder using specified input.
    """
    sfm = model.initialize(data)
    forecast = sfm.run()
    return forecast
