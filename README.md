# ramsis.datamodel

This namspace distribution implements the _RT-RAMSIS_ data model. Data is kept
persistant by means of the `sqlalchemy` object relational mapper.

## Installation

```bash
$ pip install git+https://gitlab.seismo.ethz.ch/indu/ramsis.datamodel.git
```

**Note**: It is strongly recommended to install this distribution into an
isolated (_virtual_) Python environment. A virtual environment is created by
means of e.g. the [virtualenv](https://pypi.python.org/pypi/virtualenv) package.

## License

`ramsis` namespace distributions are licensed under the [*AGPL* license]
(https://gitlab.seismo.ethz.ch/indu/rt-ramsis/blob/master/LICENSE) to be
compatible with some of the libraries we use.

## Copyright

Copyright (c) 2015-2022, Swiss Seismological Service, ETH Zurich and
Geo-Energie Suisse AG, Zurich
